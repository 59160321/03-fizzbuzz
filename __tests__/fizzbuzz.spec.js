// npm test
// npm run git -- "comment"
test('Checking', () => {
    //Arrange เตรียมการ
    //Act กระทำ
    //Assert ตรวจสอบ
    expect(true).toBe(true)
})

function fizzbuzz(num) {
    var result = ""
    if (num % 3 === 0 && num % 5 === 0) return "FizzBuzz"
    else if(num % 3 === 0) return "Fizz"
    else if(num % 5 === 0) return"Buzz"
    else return ""+num+""
}

test('CallFizzBuzz 1 to 1', () => {
    //Arrange เตรียมการ
    var param = 1
    //Act กระทำ
    var got = fizzbuzz(param)
    //Assert ตรวจสอบ
    expect(got).toBe('1')
})

test('CallFizzBuzz 2 to 2', () => {
    //Arrange เตรียมการ
    var param = 2
    //Act กระทำ
    var got = fizzbuzz(param)
    //Assert ตรวจสอบ
    expect(got).toBe('2')
})

test('CallFizzBuzz 3 to Fizz', () => {
    //Arrange เตรียมการ
    var param = 3
    //Act กระทำ
    var got = fizzbuzz(param)
    //Assert ตรวจสอบ
    expect(got).toBe('Fizz')
})

test('CallFizzBuzz 4 to 4', () => {
    //Arrange เตรียมการ
    var param = 4
    //Act กระทำ
    var got = fizzbuzz(param)
    //Assert ตรวจสอบ
    expect(got).toBe('4')
})

test('CallFizzBuzz 5 to Buzz', () => {
    //Arrange เตรียมการ
    var param = 5
    //Act กระทำ
    var got = fizzbuzz(param)
    //Assert ตรวจสอบ
    expect(got).toBe('Buzz')
})

test('CallFizzBuzz 6 to Fizz', () => {
    //Arrange เตรียมการ
    var param = 6
    //Act กระทำ
    var got = fizzbuzz(param)
    //Assert ตรวจสอบ
    expect(got).toBe('Fizz')
})

test('CallFizzBuzz 7 to 7', () => {
    //Arrange เตรียมการ
    var param = 7
    //Act กระทำ
    var got = fizzbuzz(param)
    //Assert ตรวจสอบ
    expect(got).toBe('7')
})

test('CallFizzBuzz 8 to 8', () => {
    //Arrange เตรียมการ
    var param = 8
    //Act กระทำ
    var got = fizzbuzz(param)
    //Assert ตรวจสอบ
    expect(got).toBe('8')
})

test('CallFizzBuzz 9 to Fizz', () => {
    //Arrange เตรียมการ
    var param = 9
    //Act กระทำ
    var got = fizzbuzz(param)
    //Assert ตรวจสอบ
    expect(got).toBe('Fizz')
})

test('CallFizzBuzz 10 to Buzz', () => {
    //Arrange เตรียมการ
    var param = 10
    //Act กระทำ
    var got = fizzbuzz(param)
    //Assert ตรวจสอบ
    expect(got).toBe('Buzz')
})

test('CallFizzBuzz 15 to FizzBuzz', () => {
    //Arrange เตรียมการ
    var param = 15
    //Act กระทำ
    var got = fizzbuzz(param)
    //Assert ตรวจสอบ
    expect(got).toBe('FizzBuzz')
})

test('CallFizzBuzz 30 to FizzBuzz', () => {
    //Arrange เตรียมการ
    var param = 30
    //Act กระทำ
    var got = fizzbuzz(param)
    //Assert ตรวจสอบ
    expect(got).toBe('FizzBuzz')
})

